import uchicago.src.sim.space.Object2DGrid;

/**
 * Class that implements the simulation space of the rabbits grass simulation.
 * @author 
 */

public class RabbitsGrassSimulationSpace {
	private Object2DGrid grassSpace;
	private Object2DGrid agentSpace;
	
	public static final int MAX_GRASS = 64;

	public RabbitsGrassSimulationSpace(int xSize, int ySize){
		grassSpace = new Object2DGrid(xSize, ySize);
		agentSpace = new Object2DGrid(xSize, ySize);

		for(int i = 0; i < xSize; i++){
			for(int j = 0; j < ySize; j++){
				grassSpace.putObjectAt(i, j, new Integer(0));
			}
		}
	}

	public void spreadGrass(int grass) {		
		for (int i = 0; i < grass; i++) {
			int x = (int) (Math.random() * grassSpace.getSizeX());
			int y = (int) (Math.random() * grassSpace.getSizeY());

			int j = Math.min(getGrassAt(x, y), MAX_GRASS - 1);
			grassSpace.putObjectAt(x, y, new Integer(j + 1));
		}
	}

	public int getGrassAt(int x, int y) {
		if (grassSpace.getObjectAt(x, y) != null) {
			return ((Integer)grassSpace.getObjectAt(x, y)).intValue();
		}
		return 0;
	}

	public Object2DGrid getCurrentGrassSpace(){
		return grassSpace;
	}

	public Object2DGrid getCurrentAgentSpace() {
		return agentSpace;
	}

	public boolean isCellOccupied(int x, int y){
		return agentSpace.getObjectAt(x, y) != null;
	}

	public boolean addAgent(RabbitsGrassSimulationAgent agent){
		int count = 0;
		int countLimit = 10 * agentSpace.getSizeX() * agentSpace.getSizeY();
		
		while (count < countLimit) {
			int x = (int)(Math.random() * (agentSpace.getSizeX()));
			int y = (int)(Math.random() * (agentSpace.getSizeY()));
			
			if (isCellOccupied(x, y) == false) {
				agentSpace.putObjectAt(x, y, agent);
				agent.setXY(x, y);
				agent.setRabbitsGrassSimulationSpace(this);
				return true;
			}
			count++;
		}
		return false;
	}
	
	public void removeAgentAt(int x, int y) {
		agentSpace.putObjectAt(x, y, null);
	}
	
	public int takeGrassAt(int x, int y) {
		int grass = getGrassAt(x, y);
		grassSpace.putObjectAt(x, y, new Integer(0));
		return grass;
	}
	
	public boolean moveAgentAt(int x, int y, int newX, int newY) {
		if (!isCellOccupied(newX, newY)) {
			RabbitsGrassSimulationAgent agent = (RabbitsGrassSimulationAgent)agentSpace.getObjectAt(x, y);
			removeAgentAt(x, y);
			agent.setXY(newX, newY);
			agentSpace.putObjectAt(newX, newY, agent);
			return true;
		}
		return false;
	}
	
	public int getTotalGrass() {
		int totalGrass = 0;
		for (int i = 0; i < agentSpace.getSizeX(); i++) {
			for (int j = 0; j < agentSpace.getSizeY(); j++) {
				totalGrass += getGrassAt(i, j);
			}
		}
		return totalGrass;
	}
}
