import java.awt.Color;
import java.net.DatagramSocketImplFactory;
import java.util.ArrayList;

import uchicago.src.sim.analysis.DataSource;
import uchicago.src.sim.analysis.OpenSequenceGraph;
import uchicago.src.sim.analysis.Sequence;
import uchicago.src.sim.engine.BasicAction;
import uchicago.src.sim.engine.Schedule;
import uchicago.src.sim.engine.SimModelImpl;
import uchicago.src.sim.event.SliderListener;
import uchicago.src.sim.gui.ColorMap;
import uchicago.src.sim.gui.DisplaySurface;
import uchicago.src.sim.gui.Object2DDisplay;
import uchicago.src.sim.gui.Value2DDisplay;
import uchicago.src.sim.util.SimUtilities;
import uchicago.src.sim.engine.SimInit;

/**
 * Class that implements the simulation model for the rabbits grass
 * simulation.  This is the first class which needs to be setup in
 * order to run Repast simulation. It manages the entire RePast
 * environment and the simulation.
 *
 * @author 
 */



public class RabbitsGrassSimulationModel extends SimModelImpl {
	private static final int NUMAGENTS = 100;
	private static final int WORLDXSIZE = 20;
	private static final int WORLDYSIZE = 20;
	private static final int TOTALGRASS = 1000;
	private static final int RABBITENERGY = 10;
	private static final int REPRODUCTION_ENERGY = 20;
	private static final int GRASS_GROWTH_RATE = 100;

	private Schedule schedule;
	private RabbitsGrassSimulationSpace rgsSpace;
	private DisplaySurface displaySurf;
	private OpenSequenceGraph plots;
	
	class Incrementer extends SliderListener {
		String name;
		
		public Incrementer(String initialName) {
			super();
			name = initialName;
		}
		
		public void execute() {
			switch (name) {
			case "numAgents":
				numAgents = value;
				break;
			case "worldXSize":
				worldXSize = value;
				break;
			case "worldYSize":
				worldYSize = value;
				break;
			case "reproductionEnergy":
				reproductionEnergy = value;
				break;
			case "grassGrowthRate":
				grassGrowthRate = value;
				break;
			}
		}
		
		public Incrementer setDefVal(int value) {
			setFirstVal(value);
			return this;
		}
	};
	
	class GrassInSpace implements DataSource, Sequence {
		@Override
		public Object execute() {
			return new Double(getSValue());
		}
		
		@Override
		public double getSValue() {
			return (double)rgsSpace.getTotalGrass(); 
		}
	}
	
	class RabbitsInSpace implements DataSource, Sequence {
		@Override
		public Object execute() {
			return new Double(getSValue());
		}
		
		@Override
		public double getSValue() {
			return (double)agentList.size(); 
		}
	}

	private int numAgents = NUMAGENTS;
	private int worldXSize = WORLDXSIZE;
	private int worldYSize = WORLDYSIZE;
	private int grass = TOTALGRASS;	
	private int initialRabbitEnergy = RABBITENERGY;
	private int reproductionEnergy = REPRODUCTION_ENERGY;
	private int grassGrowthRate = GRASS_GROWTH_RATE;
	private ArrayList<RabbitsGrassSimulationAgent> agentList;

	public static void main(String[] args) {
		SimInit init = new SimInit();
		RabbitsGrassSimulationModel model = new RabbitsGrassSimulationModel();
		init.loadModel(model, "", false);
	}

	public void begin() {
		buildModel();
		buildSchedule();
		buildDisplay();

		displaySurf.display();
		plots.display();
	}

	public void buildModel() {
		System.out.println("Running build model");

		rgsSpace = new RabbitsGrassSimulationSpace(worldXSize, worldYSize);
		rgsSpace.spreadGrass(grass);
		for(int i = 0; i < numAgents; i++){
			addNewAgent();
		}
	}

	private void addNewAgent(){
		RabbitsGrassSimulationAgent a = new RabbitsGrassSimulationAgent(initialRabbitEnergy);
		
		if (rgsSpace.addAgent(a)) {
			agentList.add(a);
		}
	}
	
	private int reapDeadAgents() {
		int count = 0;
		
		for (int i = (agentList.size() - 1); i >= 0; i--) {
			RabbitsGrassSimulationAgent cda = (RabbitsGrassSimulationAgent) agentList.get(i);
			
			if (cda.getEnergy() < 1) {
				rgsSpace.removeAgentAt(cda.getX(), cda.getY());
				agentList.remove(i);
				count++;
			}
		}
		return count;
	}
	
	private void reproduceRabbits() {
		for (int i = (agentList.size() - 1); i >= 0; i--) {
			RabbitsGrassSimulationAgent cda = (RabbitsGrassSimulationAgent) agentList.get(i);
			if (cda.getEnergy() > reproductionEnergy) {
				cda.setEnergy(cda.getEnergy() - initialRabbitEnergy);
				addNewAgent();
			}
		}
	}

	public void buildSchedule() {
		System.out.println("Running BuildSchedule");
		class CarryDropStep extends BasicAction {
			public void execute() {
				SimUtilities.shuffle(agentList);
				for(int i = 0; i < agentList.size(); i++){
					RabbitsGrassSimulationAgent cda = (RabbitsGrassSimulationAgent)agentList.get(i);
					cda.step();
				}
				reapDeadAgents();
				reproduceRabbits();
				rgsSpace.spreadGrass(grassGrowthRate);
				
				displaySurf.updateDisplay();
			}
		}
		schedule.scheduleActionBeginning(0, new CarryDropStep());
		
		class RGSUpdateGrassInSpace extends BasicAction {
			@Override
			public void execute() {
				plots.step();
			}
		}
		schedule.scheduleActionAtInterval(10.0, new RGSUpdateGrassInSpace());
	}

	public void buildDisplay() {
		ColorMap map = new ColorMap();

		for (int i = 1; i < 32; i++) {
			map.mapColor(i, new Color(0, i * 4 + 127, 0));
		}
		map.mapColor(0, Color.GRAY);

		Value2DDisplay displayGrass = 
				new Value2DDisplay(rgsSpace.getCurrentGrassSpace(), map);

		Object2DDisplay displayAgents = new Object2DDisplay(rgsSpace.getCurrentAgentSpace());
		displayAgents.setObjectList(agentList);

		displaySurf.addDisplayableProbeable(displayGrass, "Grass");
		displaySurf.addDisplayableProbeable(displayAgents, "Agents");
		
		plots.addSequence("Grass in space", new GrassInSpace());
		plots.addSequence("Rabbits in space", new RabbitsInSpace());
	}

	public String getName() {
		return "RabbitGrassSimulationModel";
	}

	public void setup() {
		agentList = new ArrayList<RabbitsGrassSimulationAgent>();
		schedule = new Schedule(1);
		rgsSpace = null;

		if (displaySurf != null){
			displaySurf.dispose();
		}
		displaySurf = null;
		if (plots != null) {
			plots.dispose();
		}
		plots = null;
		
		displaySurf = new DisplaySurface(this, "Rabbit Grass Model Window 1");
		plots = new OpenSequenceGraph("Amount of grass in space", this);

		registerDisplaySurface("Rabbit Grass Model Window 1", displaySurf);
		registerMediaProducer("Plot", plots);
		//initializeUIComponents();
	}

	private void initializeUIComponents() {
		modelManipulator.addSlider("NumAgents", 2, 100, 10, new Incrementer("numAgents").setDefVal(NUMAGENTS));
		modelManipulator.addSlider("WorldXSize", 10, 200, 20, new Incrementer("worldXSize").setDefVal(WORLDXSIZE));
		modelManipulator.addSlider("WorldYSize", 10, 200, 20, new Incrementer("worldYSize").setDefVal(WORLDYSIZE));
		modelManipulator.addSlider("ReproductionEnergy", 10, 1000, 100, new Incrementer("reproductionEnergy").setDefVal(REPRODUCTION_ENERGY));
		modelManipulator.addSlider("GrassGrowthRate", 1, 1000, 100, new Incrementer("grassGrowthRate").setDefVal(GRASS_GROWTH_RATE));
	}

	public String[] getInitParam(){
		String[] initParams = { "NumAgents", "WorldXSize", "WorldYSize", "Grass", "InitialRabbitEnergy", "ReproductionEnergy", "GrassGrowthRate" };
		return initParams;
	}

	public Schedule getSchedule(){
		return schedule;
	}

	public int getNumAgents(){
		return numAgents;
	}

	public void setNumAgents(int na){
		numAgents = na;
	}

	public int getWorldXSize(){
		return worldXSize;
	}

	public void setWorldXSize(int wxs){
		worldXSize = wxs;
	}

	public int getWorldYSize(){
		return worldYSize;
	}

	public void setWorldYSize(int wys){
		worldYSize = wys;
	}

	public int getGrass() {
		return grass;
	}

	public void setGrass(int i) {
		grass = i;
	}
	
	public int getReproductionEnergy() {
		return reproductionEnergy;
	}

	public void setReproductionEnergy(int reproductionEnergy) {
		this.reproductionEnergy = reproductionEnergy;
	}

	public int getGrassGrowthRate() {
		return grassGrowthRate;
	}

	public void setGrassGrowthRate(int grassGrowthRate) {
		this.grassGrowthRate = grassGrowthRate;
	}
}
