import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

import uchicago.src.sim.gui.Drawable;
import uchicago.src.sim.gui.SimGraphics;
import uchicago.src.sim.space.Object2DGrid;


/**
 * Class that implements the simulation agent for the rabbits grass simulation.

 * @author
 */

public class RabbitsGrassSimulationAgent implements Drawable {	
	private int x;
	private int y;
	private int vX;
	private int vY;
	private int energy;
	private static int IDNumber = 0;
	private int ID;
	private RabbitsGrassSimulationSpace rgsSpace;

	public RabbitsGrassSimulationAgent(int initialEnergy) {
		x = -1;
		y = -1;
		energy = 10;
		setVxVy();
		IDNumber++;
		ID = IDNumber;
		energy = initialEnergy;
	}
	
	public void setVxVy() {
		vX = 0;
		vY = 0;
		while (!((vX == 0 && vY != 0) || (vX != 0 && vY == 0))) {
			vX = (int) Math.floor(Math.random() * 3) - 1;
			vY = (int) Math.floor(Math.random() * 3) - 1;
		}
	}

	public void setXY(int newX, int newY){
		x = newX;
		y = newY;
	}
	
	public void setRabbitsGrassSimulationSpace(RabbitsGrassSimulationSpace rgss) {
		rgsSpace = rgss;
	}

	public String getID(){
		return "A-" + ID;
	}

	public int getEnergy(){
		return energy;
	}
	
	public void setEnergy(int val) {
		energy = val;
	}
	
	public void report() {
		System.out.println(getID() + " at " + x + ", " + y + " has " + getEnergy() + " energy left.");
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void draw(SimGraphics g) {
		if (energy > 0) {
			g.drawOval(Color.WHITE);
		}
	}

	public void step(){		
		int newX = x + vX;
		int newY = y + vY;
		
		Object2DGrid grid = rgsSpace.getCurrentAgentSpace();
		newX = (newX + grid.getSizeX()) % grid.getSizeX();
		newY = (newY + grid.getSizeY()) % grid.getSizeY();
		
		if (tryMove(newX, newY)) {
			energy += rgsSpace.takeGrassAt(x, y);
			rgsSpace.takeGrassAt(x, y);
		}
		else {
			setVxVy();
		}
		
		energy--;
	}
	
	private boolean tryMove(int newX, int newY) {
		return rgsSpace.moveAgentAt(x, y, newX, newY);
	}
}
